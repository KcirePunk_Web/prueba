import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public $$header;

  constructor() {}

  ngOnInit(): void {
    // this.$$header = document.querySelector('.js-header');
    window.addEventListener('scroll', this.throttle(this.onScroll, 25));
    console.log(this.$$header);
  }

  onScroll() {
    if (window.pageYOffset) {
      document.querySelector('.js-header').classList.add('is-active');
    } else {
      document.querySelector('.js-header').classList.remove('is-active');
    }
  }

  throttle(fn, delay) {
    let last;
    let timer;

    return () => {
      const now = +new Date();

      if (last && now < last + delay) {
        clearTimeout(timer);

        timer = setTimeout(() => {
          last = now;
          fn();
        }, delay);
      } else {
        last = now;
        fn();
      }
    };
  }
}
