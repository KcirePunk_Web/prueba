import { Component, OnInit } from '@angular/core';

declare var $;
@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
})
export class SliderComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    $(document).ready(function ($) {
      $('#example1').sliderPro({
        width: window.innerWidth - 50,
        height: '80vh',
        arrows: true,
        buttons: false,
        waitForLayers: true,
        thumbnailWidth: 200,
        thumbnailHeight: 100,
        thumbnailPointer: true,
        autoplay: false,
        autoScaleLayers: false,
        breakpoints: {
          500: {
            thumbnailWidth: 120,
            thumbnailHeight: 50,
          },
        },
      });
    });
  }
}
