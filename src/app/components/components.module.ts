import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from './slider/slider.component';
import { MainComponent } from './main/main.component';
import { SocialComponent } from './social/social.component';

@NgModule({
  declarations: [SliderComponent, MainComponent, SocialComponent],
  exports: [SliderComponent, MainComponent, SocialComponent],
  imports: [CommonModule],
})
export class ComponentsModule {}
